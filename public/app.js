// Import funkcji związanych z obsługą Firebase'a
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js";
import {
    getDatabase,
    push,
    ref,
    onValue
} from "https://www.gstatic.com/firebasejs/9.17.1/firebase-database.js";
// Przygtowanie konfiguracji projektu Firebase'a
// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDnTLV9AY-f6APkJDzUBzn9gXVePVUMP6Y",
    authDomain: "zdfronpol-michal.firebaseapp.com",
    databaseURL: "https://zdfronpol-michal-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "zdfronpol-michal",
    storageBucket: "zdfronpol-michal.appspot.com",
    messagingSenderId: "63466115698",
    appId: "1:63466115698:web:b8295525e402d030c5a8eb"
};


// Uruchomienie aplikacji Firebase oraz potrzebnych nam modułów
// - uruchomienie aplikacji
const app = initializeApp(firebaseConfig);
// - uruchomienie modułu Realtime Database
const database = getDatabase(app);

// Definiowanie elementów UI
const nameInput = document.querySelector("#nameInput");
const messageInput = document.querySelector("#messageInput");
const sendMessageBtn = document.querySelector("#sendMessageBtn");
const chatArea = document.querySelector("#chatArea");

// 1. Dodanie możliwości wysyłania wiadomości
const sendMessage = () => {
    // 1.1. Pobranie imienia użytkownika z inputa
    const authorName = nameInput.value;
    // 1.2. Pobranie wiadomości z inputa
    const messageName = messageInput.value;

    // 1.3. Zapisanie imienia oraz wiadomości w bazie danych
    // push -> Dodanie do bazy z automatycznie nadanym kluczem
    // push(gdzie?, co (jaki obiekt)?)
    push(ref(database, 'messages'), {
        author: authorName,
        message: messageName
    });
};
// 1.4. Podpięcie pod przycisk funkcji wysyłającej dane do bazy
sendMessageBtn.addEventListener("click", sendMessage);

// 2. Dodanie możliwości odczytu wiadomości
// 2.1. Dodanie nasłuchiwania na zmiany pod ścieżką 'messages'
// onValue(na co ma nasłuchiwać?, co robić jak coś się zmieni?)
onValue(ref(database, 'messages'), (snapshot) => {
    // 2.2. Wygenerować widok HTML ze wszystkimi wiadomościami
    let chatContent = '';

    Object.values(snapshot.val()).forEach(message => {
        chatContent += `${message.author}: ${message.message}<br>`;
    });

    // 2.3. Umieszczenie wiadomości na stronie
    chatArea.innerHTML = chatContent;
});