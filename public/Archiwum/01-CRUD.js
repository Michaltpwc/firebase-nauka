// Import funkcji związanych z obsługą Firebase'a
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js";
import { 
    getFirestore,
    doc,
    getDoc,
    setDoc,
    addDoc,
    collection,
    query,
    getDocs,
    where,
    orderBy,
    limit,
    updateDoc,
    deleteDoc
} from "https://www.gstatic.com/firebasejs/9.17.1/firebase-firestore.js";

// Przygtowanie konfiguracji projektu Firebase'a
const firebaseConfig = {
    apiKey: "AIzaSyAYZwa5wedKN6oWfJXrvrMZDkiGeeuXzTQ",
    authDomain: "zdfronpol20-arek.firebaseapp.com",
    projectId: "zdfronpol20-arek",
    storageBucket: "zdfronpol20-arek.appspot.com",
    messagingSenderId: "159590018331",
    appId: "1:159590018331:web:e450aa4c0f224547c977be"
};

// Uruchomienie aplikacji Firebase oraz potrzebnych nam modułów
// - uruchomienie aplikacji
const app = initializeApp(firebaseConfig);
// - uruchomienie modułu Firestore (baza danych)
const database = getFirestore(app);

/*
// CRUD -> Create, Read, Update, Delete

// READ: pobieranie danych z bazy danych

// OPERACJA 1: Pobieranie konkretnego dokumentu (w naszym przypadku produktu)
// 1.1: Stworzenie wskaźnika, o jaki element nam chodzi
const docRef = doc(database, "produkty", "mysz C7");
const docSnap = await getDoc(docRef);
// console.log(docSnap.data()); -> Pokazuje wszystkie pola z produktu
// console.log(docSnap.data().cena); -> Pokazuje tylko cenę produktu

// 1.2: Sprawdzenie, czy dokument istnieje? 
//      Dobry zwyczaj, gdy wyciągamy jeden konkretny dokument.
if (docSnap.exists()) {
    console.log(docSnap.data());
} else {
    console.log("Dokument nie istnieje!");
}

// CREATE: tworzenie i zapis danych do bazy danych
// OPERACJA 2: Dodanie pojedynczego dokumentu do bazy danych
// 2.1: Utworzenie ręczne wskaźnika (ścieżki), gdzie chcemy zapisać dokument
// Dodaje (jeśli nie istnieje) lub nadpisuje (gdy wpis już istniał)
const setDocRef = doc(database, "produkty", "monitor e2");
const monitorProperties = {
    nazwa: "Monitor przenośny e2",
    cena: 500,
    czy_dostepny: false
};
//await setDoc(setDocRef, monitorProperties);

// 2.2: Utworzenie automatyczne wskaźnika (ścieżki), gdzie chcemy zapisać dokument
// Dodaje za każdym razem nowy wpis
const productsCollectionRef = collection(database, "produkty");
const webcamProperties = {
    nazwa: "Webcam v2",
    cena: 200,
    czy_dostepny: true
}
const createdProduct = await addDoc(productsCollectionRef, webcamProperties);
const createdProductId = createdProduct.id;

const createdProductRef = doc(database, "produkty", createdProductId);
const createdProductSnap = await getDoc(createdProductRef);
console.log(createdProductSnap.data());

// READ ALL: pobieranie wszystkich elementów z kolekcji
// OPERACJA 3: odczytanie wszystkich elementów z kolekcji

// 3.1. Wskazanie kolekcji, z której chcemy pobrać dane
const queryAllProducts = query(collection(database, "produkty"));
// const queryAllProducts = query(productsCollectionRef); -> to jest to samo, co wyżej
const allProductsSnap = await getDocs(queryAllProducts);
/*
allProductsSnap.forEach(singleProduct => {
    console.log(singleProduct.data().nazwa);
});


// OPERACJE 4: odczytanie elementów z kolekcji, które spełniają nasz warunek
const queryNotAvailableProducts = query(collection(database, "produkty"),
                                    where("czy_dostepny", "==", false));
                                    // where('nazwa_pola', 'operator', oczekiwana_wartość)
const allNotAvailableProducts = await getDocs(queryNotAvailableProducts);
allNotAvailableProducts.forEach(notAvaiableProduct => {
    console.log(notAvaiableProduct.data().nazwa);
});

// OPERACJE 5: sortowanie wyników z kolekcji
const queryAllProductsSortedByPriceAscending = query(collection(database, "produkty"),
                                    orderBy("cena", "asc"),
                                    limit(3));
                                    // orderBy(po_czym_sortujemy, rosnąco_czy_malajęco (asc, desc))
const allProductsSortedByPriceAsceding = await getDocs(queryAllProductsSortedByPriceAscending);
allProductsSortedByPriceAsceding.forEach(product => {
    console.log(`${product.data().nazwa}: ${product.data().cena} PLN`);
})


// Update: aktualizowanie danych w bazie danych
// OPERACJA 6: zaktualizujmy dane w kolekcji
const productToUpdateRef = doc(database, "produkty", "laptop B12");
const productToUpdateProperties = {
    cena: 1200,
    czy_dostepny: true
};
//await updateDoc(productToUpdateRef, productToUpdateProperties);

// Delete: usuwanie danych z bazy danych
// OPERACJA 7: usuńmy dane z kolekcji
const productToDeleteRef = doc(database, "produkty", "0SLKYXucWoD4z6tzKfD1");
//await deleteDoc(productToDeleteRef);

// ---------------------------------------------------------------

// OPERACJA 8: Wyświetlenie dostępnych produktów na stronie oraz ich cen

// 8.1: Pobranie dostępnych produktów z bazy danych
const queryAllAvailableProducts = query(collection(database, "produkty"),
                                    where("czy_dostepny", "==", true));
const allAvailableProducts = await getDocs(queryAllAvailableProducts);

// 8.2: Wygenerowanie widoku HTML
let availableProductsList = "<ul>";
allAvailableProducts.forEach(product => {
    availableProductsList += `<li>${product.data().nazwa}: ${product.data().cena} PLN</li>`
});
availableProductsList += "</ul>";

// 8.3: Umieszczenie widoku na stronie
const availableProductsDiv = document.querySelector("#availableProducts");
availableProductsDiv.innerHTML = availableProductsList;

// --------------------------------------------------

// OPERACJA 9: Złożone zapytanie bazujące na indeksach
// 9.1: Pobranie dostępnych produktów o cenie < 200
const queryAllAvailableProductsAndPriceLessThan200 = query(collection(database, "produkty"),
                                                        where("czy_dostepny", "==", true),
                                                        where("cena", "<", 200));
const allAvailableProductsAndPriceLessThan200 = await getDocs(queryAllAvailableProductsAndPriceLessThan200);

// 9.2: Wydrukowanie w konsoli tych produktów
allAvailableProductsAndPriceLessThan200.forEach(product => {
    console.log(product.data());
})


*/







const generateAllAnimalsView = async () => {
    // Zadanie 14
    // 14.1: Pobranie wszystkich zwierząt z bazy danych
    const queryAllAnimals = query(collection(database, "zwierzeta"));
    const allAnimals = await getDocs(queryAllAnimals);

    // Jeśli chcemy zamienić na tablicę:
    console.log(typeof(allAnimals)); // -> object
    const zapytanieZamienioneWTablice = allAnimals.docs.map(x => x.nazwa = 'abc'); // -> zamiana na tablicę
    console.log(zapytanieZamienioneWTablice);

    // 14.2: Wygenerowanie fragmentu HTML
    let allAnimalsView = "<ol>";

    // 16.1: Wyświetlenie ceny w postaci Inputa ✅
    allAnimals.forEach(animal => {
        allAnimalsView += 
        `<li>
        ${animal.data().nazwa}: 
        <input type="number" data-id=${animal.id} value=${animal.data().cena} class="inputPriceUpdate">
        <button type="button" data-id=${animal.id} class="btnDelete">Usuń</button>
        </li>`;
    });

    allAnimalsView += "</ol>";

    // 14.3: Wrzucenie stworzonego widoku na stronę
    const allAnimalsDiv = document.querySelector("#allAnimals");
    allAnimalsDiv.innerHTML = allAnimalsView;

    // 15.2.2: Wyłapanie przycisków z HTMLa ✅
    const btnsDelete = document.querySelectorAll(".btnDelete");

    // 15.2.4: Dodanie akcji usuwania do przycisków ✅
    btnsDelete.forEach(btn => {
        btn.addEventListener("click", deleteAnimalFromDatabse);
    });

    // 16.3: Dodanie do Inputa eventu, który będzie reagował na zmianę wartości
    // 16.3.1: Znalezienie wszystkich inputów ✅
    const inputsUpdate = document.querySelectorAll(".inputPriceUpdate");
    
    // 16.3.2: Dodanie eventów do Inputów ✅
    inputsUpdate.forEach(input => {
        input.addEventListener("change", updatePriceInDatabase);
    })
};

// 15.2.3: Dodanie funkcji odpowiedzialnej za usuwanie zwierząt z bazy ✅
const deleteAnimalFromDatabse = async (event) => {
    const animalId = event.target.dataset.id;
    const animalRef = doc(database, "zwierzeta", animalId);
    await deleteDoc(animalRef);

    generateAllAnimalsView();
};

// 16.2: Napisanie funkcjonalności, która aktualizuje cenę w bazie ✅
const updatePriceInDatabase = async (event) => {
    const animalId = event.target.dataset.id;
    const animalRef = doc(database, "zwierzeta", animalId);

    const newPrice = Number(event.target.value);
    const updatedAnimal = {
        cena: newPrice
    };

    await updateDoc(animalRef, updatedAnimal);
};

generateAllAnimalsView();

// Zadanie 15 ✅
// 15.1: Dodanie przycisku "Usuń" obok ceny ✅

// 15.2: Dodanie do każdego przycisku akcji usuwania danego zwierzęcia ✅

// 15.2.1: Dodanie klasy do przycisków, żeby potem złapać je przez JS ✅

// Zadanie 16 ✅

