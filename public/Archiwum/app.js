import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js";

import {
    getStorage,
    uploadBytesResumable,
    ref,
    getDownloadURL,
    listAll,
    uploadBytes
} from "https://www.gstatic.com/firebasejs/9.17.1/firebase-storage.js";

// Przygtowanie konfiguracji projektu Firebase'a
const firebaseConfig = {
    apiKey: "AIzaSyDnTLV9AY-f6APkJDzUBzn9gXVePVUMP6Y",
    authDomain: "zdfronpol-michal.firebaseapp.com",
    projectId: "zdfronpol-michal",
    storageBucket: "zdfronpol-michal.appspot.com",
    messagingSenderId: "63466115698",
    appId: "1:63466115698:web:b8295525e402d030c5a8eb"
};



// Uruchomienie aplikacji Firebase oraz potrzebnych nam modułów
// - uruchomienie aplikacji
const app = initializeApp(firebaseConfig);
// - uruchomienie modułu Storage
const storage = getStorage(app);

// Definiowanie elementów UI
const imageInput = document.querySelector("#imageInput");
const imageName = document.querySelector("#imageName");
const uploadImageBtn = document.querySelector("#uploadButton");
const uploadImageProgressBar = document.querySelector("#uploadImageProgressBar");
const uploadedImage = document.querySelector("#uploadedImage");
const imagesDropDownList = document.querySelector("#imagesDropDownList");
const selectedImage = document.querySelector("#selectedImage");
const audioPlayer = document.querySelector("#audioPlayer");
// ----- WRZUCANIE PLIKU Z DYSKU LOKALNEGO NA SERWER -----
// 1. Zmienne pomocnicze do przechowania pliku
let fileToUpload;
let fileExtension;

// 2. Obsługa procesu wyboru pliku
// 2.1. Wbijamy się w moment, kiedy plik zostanie wybrany,
//      więc event 'change' zostanie wywołany
imageInput.onchange = (event) => {

    // 2.2. Zapamiętanie całego pliku, który wrzucimy na Storage
    fileToUpload = event.target.files[0];

    // 2.3. Pobranie pełnej nazwy wybranego przez użytkownika pliku
    const fullFileName = event.target.files[0].name;

    // 2.4. Podział pełnej nazwy wybranego pliku na nazwę oraz rozszerzenie pliku
    // a.b.c.jpg -> ["nazwa pliku", "jpg"]
    const splittedFileName = fullFileName.split('.');

    // 2.5. Pobranie samego rozszerzenia pliku
    fileExtension = splittedFileName[splittedFileName.length - 1];
};

// 3. Przesyłanie wybranego obrazka do Firebase (Storage)
uploadImageBtn.onclick = () => {

    // 3.1. Pobranie nazwy docelowego pliku z inputa
    const fileName = imageName.value;

    // 3.2. Złączenie docelowej nazwy pliku z rozszerzeniem pliku
    // Plik musi mieć pełną nazwę, tzn. nazwa + rozszerzenie (np. obrazek.jpg)
    const fullFileName = `${fileName}.${fileExtension}`

    // 3.3. Określenie metadanych
    const metaData = {
        contentType: fileToUpload.type
    };

    // 3.4. Komunikacja ze Storage w celu wrzucenia pliku
    const uploadProcess = uploadBytesResumable(ref(storage, `images/${fullFileName}`), fileToUpload, metaData);

    // 3.5. Wbijamy się w moment, kiedy status wrzucania pliku będzie się zmieniał
    // Składania Callback -> Promise -> async/await
    uploadProcess.on("state_changed",
        // Co robić w trakcie przesyłania?
        (snapshot) => {
            const progress = snapshot.bytesTransferred / snapshot.totalBytes * 100;
            uploadImageProgressBar.innerHTML = `${Math.round(progress)}%`;
        },
        // Co robić kiedy błąd podczas przesyłania?
        (error) => {
            console.error(error);
        },
        // Co robić kiedy proces zakończy się sukcesem?
        async () => {

            // 3.6. Pobranie URL pliku
            const imageUrl = await getDownloadURL(uploadProcess.snapshot.ref);

            // 3.7. Umieszczanie obrazka na stronie
            uploadedImage.setAttribute("src", imageUrl);

        }
    );
};

// ----- ODCZYT PLIKÓW Z SERWERA -----
// 4. Pobranie konkretnego obrazka

// 4.1. Pobranie URL obrazka
const imageUrl = await getDownloadURL(ref(storage, "images/deadmau5.jpg"));

// 4.2. Umieszczenie obrazka na stronie
uploadedImage.setAttribute("src", imageUrl);

/*
// 5. Pobranie wszystkich dostępnych plików (w folderze)

// 5.1. Pobieranie referencji (wskażników) plików z konkretnego     folderu. Jako wynik otrzymamy tablicę
const allImages = await listAll(ref(storage, 'images'));

// 5.2. Przejście pętlą przez wszystkie znalezione pliki

let allImagesView = "<ul>";

// [ref1, ref2] --> [obietnica, ze dostane link1; obietnica, ze dostane link2]
const getAllImagesPromise = allImages.items.map(async (imageRef) => {
    const imageUrl = await getDownloadURL(imageRef);
    return `<li><img src="${imageUrl}"></li>`;
});

const arrayWithImagesListElements = await Promise.all(getAllImagesPromise);
arrayWithImagesListElements.forEach(imageListElement => {
    allImagesView += imageListElement;
});

allImagesView += "</ul>";

uploadImageProgressBar.innerHTML = allImagesView;
*/










// // Zadanie 7a
// // 7.1: Pobranie wszystkich obrazków
// const allImages = await listAll(ref(storage, 'images'));

// // 7.2: Utworzenie drop down listy -> HTML
// let imagesDropDownListHTML = `<select id="allImagesSelectList">`;

// // 7.3: Wrzucenie do listy rozwijanej nazw plików -> HTML
// allImages.items.forEach(imageRef => {
//     // Pobranie nazw wszystkich plików
//     const imageName = imageRef.name.split('.')[0];

//     // Wygenerowanie option dla każdego z obrazków
//     imagesDropDownListHTML += `<option value="${imageRef.fullPath}">${imageName}</option>`
// });

// // 7.4: Zamknięcie listy rozwijanej -> HTML
// imagesDropDownListHTML += "</select>";

// // 7.5: Umieszczenie listy rozwijanej na stronie
// imagesDropDownList.innerHTML = imagesDropDownListHTML;

// // 7.6: Dodanie reagowania na zmiany w rozwijaku
// imagesDropDownList.onchange = async (event) => {
//     // 7.6.1: Pobranie obrazku ze storage na bazie pełnej ścieżki
//     const selectedImageFullPath = event.target.value;
//     const imageUrl = await getDownloadURL(ref(storage, selectedImageFullPath));
//     selectedImage.setAttribute("src", imageUrl);
// };

// 8.1. Pobranie linku do pliku audio ze Storage'a
const audioUrl = await getDownloadURL(ref(storage, "audio/No Goodbye (Extended Version).mp3"));

// 8.2. Umieszczenie na stronie odtwarzacza audio -> HTML
// 8.3. Podpięcie pod odtwarzacz pobranego pliku audio
let audioPlayerHTML =
    `<audio controls>
        <source src="${audioUrl}" type="audio/mpeg">
     </audio>
    `;

// 8.4. Umieszczenie odtwarzacza na stronie
audioPlayer.innerHTML = audioPlayerHTML;
